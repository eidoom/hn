# [hn](https://gitlab.com/eidoom/hn)

An interface for HN using the [HN API](https://github.com/HackerNews/API).

Built similarly to:

* [anti-hour](https://gitlab.com/eidoom/anti-hour) 
* [stories2](https://gitlab.com/eidoom/stories2)

## Deployment

Example configuration.

### Build

```shell
npm i
npm run build
```

### Systemd service

`/etc/systemd/system/hn.service`

```systemd
[Unit]
Description=HN
After=network.target

[Service]
User=USER
Group=USER

WorkingDirectory=/PATH/TO/hn
Environment="HOST=127.0.0.1"
Environment="PORT=3001"
Environment="PROTOCOL_HEADER=x-forwarded-proto"
Environment="HOST_HEADER=x-forwarded-host"
ExecStart=node build

Type=simple
TimeoutStopSec=20
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

```shell
sudo systemctl enable hn.service
sudo systemctl start hn.service
```

### Nginx reverse proxy

`/etc/nginx/sites-available/hn`

```nginx
server {
    listen 80;

    server_name hn.DOMAIN;

    location / {
        proxy_pass http://127.0.0.1:3001/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

```shell
ln -s /etc/nginx/sites-available/hn /etc/nginx/sites-enabled/hn
sudo systemctl reload nginx
```
