import { pluralise } from "$lib/plural.js";

const time_format = new Intl.DateTimeFormat("en-GB", {
  year: "numeric",
  month: "short",
  day: "numeric",
  hour: "numeric",
  minute: "numeric",
  hour12: false,
});

export function format_time(stamp) {
  return time_format.format(new Date(1000 * stamp));
}

export function format_ago(secondsf) {
  const seconds = Math.floor(secondsf);
  if (seconds < 60) {
    return pluralise(seconds, "second");
  }

  const minutes = Math.floor(seconds / 60);
  if (minutes < 60) {
    return pluralise(minutes, "minute");
  }

  const hours = Math.floor(minutes / 60);
  if (hours < 24) {
    return pluralise(hours, "hour");
  }

  const days = Math.floor(hours / 24);
  if (days < 7) {
    return pluralise(days, "day");
  }

  const weeks = Math.floor(days / 7);
  if (weeks <= 52) {
    return pluralise(weeks, "week");
  }

  const years = Math.floor(days / 365.25);
  return pluralise(years, "year");
}
