export const pages = ["top", "best", "new", "ask", "show", "job"];

export const api = "https://hacker-news.firebaseio.com/v0/";
export const api1 = "https://hn.algolia.com/api/v1/";

export function item_url(item, page_url) {
  return new URL(item.url ?? `${page_url.origin}/item/${item.id}`);
}

export function paginate(page_str) {
  const page = typeof page_str === "undefined" ? 0 : parseInt(page_str),
    n = 12,
    start0 = page * n,
    start = start0 + 1,
    end = start0 + n;

  return { start0, start, end, page };
}
