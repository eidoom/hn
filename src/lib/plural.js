export function pluralise(n, str) {
  return `${n} ${str}${n === 1 ? "" : "s"}`;
}

// export function pluraly(n, str) {
//   return `${n} ${str}${n === 1 ? "y" : "ies"}`;
// }
