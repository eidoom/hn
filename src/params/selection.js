import { pages } from "$lib/global.js";

export function match(param) {
  return pages.includes(param);
}
