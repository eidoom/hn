import { format_time, format_ago } from "$lib/chrono.js";
import { api, item_url, paginate } from "$lib/global.js";

export async function load({ fetch, params, url }) {
  const kind = params.kind;

  const { start0, start, end, page } = paginate(params.page);

  const is = await fetch(`${api}/${kind}stories.json`).then((r) => r.json());

  const now = Date.now() / 1000; // seconds

  const stories = await Promise.all(
    is.slice(start0, end).map(
      async (i) =>
        await fetch(`${api}/item/${i}.json`)
          .then((r) => r.json())
          .then((o) => {
            const url_obj = item_url(o, url);
            return {
              ...o,
              href: url_obj.href,
              hostname: url_obj.hostname,
              time: format_time(o.time),
              ago: format_ago(now - o.time),
              local: url_obj.hostname === url.hostname,
            };
          }),
    ),
  );

  return {
    kind,
    page,
    start,
    stories,
  };
}
