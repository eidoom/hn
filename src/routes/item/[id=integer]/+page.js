import { format_time, format_ago } from "$lib/chrono.js";
import { api1, item_url } from "$lib/global.js";

function count(o) {
  // recursively count number of descendants
  return o.children.reduce((acc, c) => acc + 1 + count(c), 0);
}

function get_next(children) {
  // add next property to each child object except last
  return [
    ...children
      .slice(0, -1)
      .map((child, i) => ({ ...child, next: children[i + 1].id })),
    children[children.length - 1],
  ];
}

function get_prev(children) {
  // add prev property to each child object except first
  return [
    children[0],
    ...children
      .slice(1)
      .map((child, i) => ({ ...child, prev: children[i].id })),
  ];
}

function translate(o, now, url) {
  // abstraction layer to translate algolia result to our format of object
  const oo = {
    ago: format_ago(now - o.created_at_i),
    by: o.author,
    id: o.id,
    time: format_time(o.created_at_i),
    type: o.type,
  };

  if (o.children.length > 0) {
    oo.descendants = count(o);
    oo.comments = o.children.map((c) => translate(c, now, url));
    oo.comments = get_next(oo.comments);
    oo.comments = get_prev(oo.comments);
  }

  if (o.url !== null) {
    const url_obj = item_url(o, url);
    oo.href = url_obj.href;
    oo.hostname = url_obj.hostname;
  }

  if (o.points !== null) {
    oo.score = o.points;
  }

  if (o.title !== null) {
    oo.title = o.title;
  }

  if (o.parent_id !== null) {
    oo.root = o.story_id;

    if (o.story_id !== o.parent_id) {
      oo.parent = o.parent_id;
    }
  }

  if (o.text !== null) {
    // replace links to hn items with the corresponding link for this ui
    oo.text = o.text.replaceAll(
      "https:&#x2F;&#x2F;news.ycombinator.com&#x2F;item?id=",
      "/item/",
    );
  }

  return oo;
}

export async function load({ fetch, params, url }) {
  const now = Date.now() / 1000; // seconds

  return {
    // use api1=algolia for single request returning all children
    item: await fetch(`${api1}/items/${params.id}`)
      .then((r) => r.json())
      .then((o) => translate(o, now, url)),
  };
}
