import { format_time, format_ago } from "$lib/chrono.js";
import { api, item_url, paginate } from "$lib/global.js";

export async function load({ fetch, params, url }) {
  const id = params.id;

  const { start0, start, end, page } = paginate(params.page);

  const user = await fetch(`${api}/user/${id}.json`)
    .then((r) => r.json())
    .then((o) => ({
      ...o,
      created: format_time(o.created),
    }));

  const now = Date.now() / 1000; // seconds

  const submitted = (
    await Promise.all(
      user.submitted
        .slice(start0, end)
        .map(
          async (i) =>
            await fetch(`${api}/item/${i}.json`).then((r) => r.json()),
        ),
    )
  )
    .filter((c) => !c.dead)
    // TODO since submitted includes comments and we filter out only stories,
    // the pagination and <ol> indices will be messed up
    .filter((s) => s.type === "story")
    .map((o) => {
      const url_obj = item_url(o, url);
      return {
        ...o,
        href: url_obj.href,
        hostname: url_obj.hostname,
        time: format_time(o.time),
        ago: format_ago(now - o.time),
        local: url_obj.hostname === url.hostname,
      };
    });

  return { start, page, user, submitted };
}
